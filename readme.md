# Projet de NF17 du Groupe 5 (TD 5) - Informatisation des activités d'une bibliothèque municipale

|Contributeurs|
|----------| 
|AMIOT Noé|
|BACHELET Antoine|
|CARREZ Emilien|
|LATROUS Emna|
|VANALDEWERELD Florentin|
|Clients|
|----------|
|VICTORINO Alessandro|

L'application de la bibilothéque doit permettre la gestion des ressources et de leurs emprunts, la gestion des comptes des adhérents, ainsi que le recueillement de données sur les documents.
Ce système doit permettre de faciliter l'accès des diverses fonctionnalités aux utilisateurs.

## Informations utiles à la compréhesion du projet : 

Une liste des commandes utiles est disponible dans le fichier [Documentation.md](https://gitlab.utc.fr/amiotnoe/nf17-td5-g5-biblio/-/blob/master/Documentation.md).

Un trigger supplémentaire peut être ajouté au projet pour maintenir les contraintes complexes.
Il n'est pas initialement inclus dans le projet car il restreindrait les tests, ce qui n'est pas soughaitable ici.
Il permettrait de s'assurer, à l'ajout d'un emprunt, que celui-ci est possible (ressource disponible, utilisateur autorisé).
```sql
CREATE FUNCTION public.trg_emprunt_valide()
RETURNS trigger LANGUAGE plpgsql
AS
$$
BEGIN
    IF NOT EXISTS (SELECT login FROM vemprunteur_potentiel WHERE vemprunteur_potentiel.login=NEW.utilisateur)
        THEN
            RAISE EXCEPTION 'Cet utilisateur n a pas le droit d emprunter';
    END IF;
    
    IF NOT EXISTS (SELECT ressource,id FROM vdisponible WHERE vdisponible.id=NEW.exemplaire AND vdisponible.ressource=NEW.ressource)
        THEN
            RAISE EXCEPTION 'Cette ressource n est pas disponible actuellement';
    END IF;
    RETURN NEW;
END
$$;

CREATE TRIGGER ispret_ok BEFORE INSERT ON public.pret FOR EACH ROW EXECUTE PROCEDURE public.trg_emprunt_valide();
```