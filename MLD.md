# Modèle Logique de Données Relationnel
*Tout est NOT NULL sauf indication contraire*
-   **Nationalité**(#nom: varchar(60)) _3NF_ 
-   **Genre**(#nom: varchar(60)) _3NF_
-   **Langue**(#nom: varchar(60)) _3NF_
-   **Editeur**(#nom: varchar(60)) _3NF_
-   **Contributeur**(#IDContrib: integer, nom: varchar(60), prenom: varchar(60),dateNaissance: Date, nationalité=>Nationalité) _3NF_ avec (Nom,prénom,dateNaissance) unique et Nom XOR Prénom non NULL
3NF car clé primaire unique et aucun attribut non clé déterminée un attribut non clé. Ici (nom,prenom,dateNaissance) n’est pas une clé, on considère que, même avec une faible probabilité, ce cas peut survenir.

#### Choix héritage pour Contributeur :

-   **Par la mère** : Beaucoup trop de contraintes car les filles possèdent chacune des associations
-   **Par les filles** : On devrait rajouter des attributs pour la nationalité dans chacune des classes filles (clé étrangère) et on aurait une contrainte avec l’intersection des tables filles qui donnent vide.
-   **Par référence** : On a une contrainte de projection et une table qui se rajoute. De plus, étant donné qu’un acteur peut être un réalisateur et un auteur, une table contributeur pourrait nous permettre de pouvoir référencer le même contributeur dans plusieurs tables filles (acteur, auteur…).  

#### Contrainte Héritage Par référence :

    Projection(Contributeur,IDContrib)=Projection(Auteur,contributeur) UNION Projection(Acteur,contributeur) UNION Projection(Realisateur, contributeur) UNION    Projection(Compositeur,contributeur) UNION Projection(Interprete,contributeur) 
    
-   **Auteur**(#contributeur =>Contributeur) _3NF_ 
-	**Acteur**(#contributeur=>Contributeur) _3NF_
-   **Realisateur**(#contributeur=>Contributeur) _3NF_
-   **Compositeur**(#contributeur=>Contributeur) _3NF_
-   **Interprète**(#contributeur=> Contributeur) _3NF_

⇒ Classes induites par la relation N:M

-   **Ecrivent**(#auteur=>Auteur, #livre=>Livre)3NF
-   **Jouent**(#acteur=>Acteur, #film=>Film)3NF
-   **Realisent**(#realisateur=>Realisateur, #film => Film)3NF 
-   **Composent**(#compositeur=>Compositeur, #musique=>Musique)3NF 
-   **Interpretent**(#contrib=>Interprete, #musique =>Musique)3NF 

⇒  Or, pour optimiser la base de données, on a fait le choix de supprimer les classes filles Auteur, Acteur, Réalisateur, Compositeur et Interprète qui n'apportent aucune informations supplémentaires à part de la redondance. On remplace donc dans les tables ci-dessus, induites par la relation N:M, la clé étrangère vers les tables filles de Contributeur par une clé étrangère vers Contributeur. De ce fait la contrainte complexe de l’héritage par référence est supprimée. On a donc au final, la table Contributeur à laquelle on ajoute les 5 tables ci-dessous: 

-   **Ecrivent**(#IDContributeur=>Contributeur, #IDLivre=>livres)
-   **Jouent**(#IDContributeur=>Contributeur, #IDFilm=>films);
-   **Realisent**(#IDContributeur=>Contributeur, #IDFilm>films)
-   **Composent**(#IDContributeur=>Contributeur, #IDMusique=>musiques)
-   **Interpretent**(#IDContributeur=>Contributeur, #IDMusique=>musiques)


-   **Ressources**(#code: int, titre: varchar(60), date_apparition: date, codeClassification: varchar(60), genre=>Genre, editeur=>Editeur) 
    
⇒ 3NF car clé primaire unique. Aucun attribut non clé détermine un attribut non clé. On considère que le code de classification est un code définissant l’étage et l’étagère, ainsi plusieurs livres pas forcément du même genre ou du même éditeur peuvent avoir le même code de classification. 

#### Héritage par référence pour Ressources :

-   **Livre**(#ressource=>Ressource, ISBN: int, resume: varchar,langue=>Langue) avec ISBN (key) 

⇒ 3NF car on a resume et langue qui dépend fonctionnellement de ISBN, or ISNB est une clé candidate. Ainsi, en plus d’avoir une clé primaire unique et une clé candidate à un attribut, aucun attribut non clé détermine un attribut non clé. 

-   **Film**(#ressource=>Ressource, longueur: int, synopsis: varchar, langue=>Langue) 

⇒ 3NF car clé primaire unique. Aucun atribut non clé détermine un autre attribut non clé. De plus, ici on peut avoir par exemple des versions longues du même film, donc le synopsis ne définit pas sa longueur. 

-   **Musique**(#ressource=>Ressource, longueur:int)

⇒ 3Nf car clé primaire unique. Aucun attribut non clé détermine un autre attribut non clé. 

#### Contrainte Héritage par référence

    Intersection(Projection(Film,ressource),Projection(Livre,ressource),Projection(Musique,ressource))={} (Un film ou livre ou une musique) 
    Projection(Ressource,code)=Projection(Livre,ressource) UNION Projection(Film,ressource) UNION Projection(Musique,ressource)

-   **Exemplaire**(#ressource=>Ressource, #IDExemplaire: int, Etat: {neuf,bon,abimé,perdu}, prix: float) _3NF_
 
⇒ 3NF car clé primaire unique. Aucun attribut non clé détermine un autre attribut non clé.

#### Contrainte Composition avec Ressource:

    IDExemplaire NOT NULL (déjà exprimé par le fait que tous les attributs sauf spécifiés sont non nuls)
    Projection(Ressource,code)=Projection(Exemplaire,ressource)
 
-    **PropriétaireCompte**(#login: varchar(60), mdp: varchar(60), nom: varchar(60), prenom:varchar(60), addPostale: varchar(120), addMail: varchar(120))
 
 __*__ (nom,prenom,addPostale), (nom,prenom,numTel) et  (nom,prenom,addMail) clés candidates (ici on considère qu’une addresse mail peut-être commune à tous les membres d’une famille, donc pas de clé plus minimale. De plus le numéro de téléphone peut être celui des parents en cas d’inscription d’enfants, donc on peut avoir plusieurs fois le même numéro.) 

⇒ 3NF car clé primaire unique, aucun attribut non clé détermine un autre attribut non clé, et aucune partie d’une clé détermine un autre attribut. 
    
   *Ici on choisit l’héritage par classe fille car celle-ci amène à une seule contrainte. De plus les filles ont des associations différentes et la mère, qui est de plus abstraite, n’en a pas. La suite est donc grandement simplifiée.  Si on avait fait par référence on aurait dû, en plus de la contrainte sur l'intersection, rajouter celle sur la projection:*
    
    Projection(ProprietaireDeCompte,login)=Projection(Utilisateur,login) UNION Projection(Personnel,login) 
    
#### Héritage par les filles pour Propriétaire Compte:

-    **Utilisateur**(#login: varchar(60), mdp: varchar(60), nom: varchar(60), prenom: varchar(60), addPostale: varchar(120), addMail: varchar(120), numTel: varchar(10), DateNaissance: Date, DateBlacklist: Date)
 
 __*__ avec DateBlacklist Nullable et avec (nom,prenom,addPostale), (nom,prenom,numTel) et (nom,prenom,addMail) clés candidates (ici on considère qu’une addresse mail peut-être commune à tous les membres d’une famille, donc pas de clé plus minimale) De plus le numéro de téléphone peut être celui des parents en cas d’inscription d’enfants, donc on peut avoir plusieurs fois le même numéro) 

⇒ 3NF car clé primaire unique, aucun attribut non clé détermine un autre attribut non clé, et aucune partie d’une clé détermine un autre attribut.
    
-   **Personnel**(#login: varchar(60), mdp: varchar(60), nom: varchar(60), prenom: varchar(60), addPostale: varchar(120), addMail: varchar(120)) 
⇒ 3NF car clé primaire unique, aucun attribut non clé détermine un autre attribut non clé, et aucune partie d’une clé détermine un autre attribut.  

 __*__ avec (nom,prenom,addPostale) et (nom,prenom,addMail) clés candidates (ici on considère qu’une addresse mail peut-être commune à tous les membres d’une famille) 3NF

#### Contrainte Heritage par fille :

    Intersection(Projection(Personnel,login),Projection(Utilisateur,login))={}

-   **Prêt**(#code_res=>Exemplaire(Ressource), #id_exemp=>Exemplaire(IDExempaire), #utilisateur=>Utilisateur, #date_debut: Date, date_fin: Date, date_rendu: Date, etatRendu:{neuf,bon,abimé,perdu}) _3NF_ 

⇒ 3NF car aucun attribut non clé détermine un autre attribut non clé, et aucune partie d’une clé détermine un autre attribut.

-   **Adhesion**(#utilisateur=>Utilisateur, #dateDebut; date, dateFin: date) _3NF_ 

⇒ 3NF car aucun attribut non clé détermine un autre attribut non clé, et aucune partie d’une clé détermine un autre attribut.

-   **Remboursement**(#pret=>pret, date_remboursement: date) _3NF_

__*__ avec date_rembousement nullable (si c’est nul c’est en attente de remboursement) 

⇒ 3NF car aucun attribut non clé détermine un autre attribut non clé, et aucune partie d’une clé détermine un autre attribut.

#### Contrainte Composition entre prêt et remboursement :

⇒ Aucun contrainte car ici un prêt peut ne pas contenir de remboursement, donc il n’est pas forcément présent dans la table Remboursement. De plus, étant donné que l’on a seulement un remboursement par prêt au plus, on a pas de clé locale pour Remboursement car l’on en n'a pas besoin, connaissant le prêt correspondant, de l’identifier uniquement puisqu’il est déjà unique dans un prêt. 

-   **Réservation**(#Utilisateur=>Utilisateur,#Ressource=>Ressource,dateReservation: date) _3NF_

# Liste des vues disponibles en plus des tables

-   **vsanction** pour connaître les sanctions en cours
-   **vretard** pour visualiser les retards et le nombre de jours de retard
-   **vemprunteur_potentiel** pour visualiser les adhérents qui ont le droit d'emprunter (donc non blacklisté, nombre d'emprunts en cours inférieur à 5, pas de sanctions en cours)
-   **vlivre** pour visualiser les livres que la bibliothèque possède
-   **vfilm** pour visualiser les films que la bibliothèque possède
-   **vmusique** pour visualiser les enregistrement musicaux que la bibliothèque possède
-   **vcontribution** permet de visualiser les contributeurs pour chaque ressource, on se sert de cette vue pour simplifier la recherche de ressource par contributeur  
-   **vadherent** permet de visualiser les adhérents de la bibliothèque
-   **vdisponible** permet de visualiser les exemplaires disponibles (non empruntés et non réservés), donc indirectement les ressources avec un group by

# Liste des triggers (déclencheurs) ajoutés
Les triggers suivants permettent de maintenir l'intégrité des données en vérifiant les contraintes complexes avant l'insertion de données dans les tables. 
-   **trg_date_not_between_check** contraint les dates d'adhésion. S'assure que les périodes d'adhésions ne se chevauchent pas.
-   **trg_ressource_film_ok** contraint les films. S'assure que la ressource qui va être ajoutée comme un film n'existe pas déjà en tant que livre ou musique (contrainte due à l'héritage sur ressource).
-   **trg_ressource_livre_ok** contraint les livres. S'assure que la ressource qui va être ajoutée comme un livre n'existe pas déjà en tant que film ou musique (contrainte due à l'héritage sur ressource).
-   **trg_ressource_musique_ok** contraint les musiques. S'assure que la ressource qui va être ajoutée comme une musique n'existe pas déjà en tant que film ou livre (contrainte due à l'héritage sur ressource).

Le trigger suivant est optionnel et trouvable dans le readme :
-   **trg_emprunt_valide** contraint les emprunts. S'assure que l'emprunt effectué l'est sur une ressource disponible et que l'utilisateur a le droit de le faire (utilise les vues vdisponible et vemprunteur_potentiel).