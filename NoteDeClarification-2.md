# Projet de NF17 du Groupe 5 (TD 5) - Informatisation des activités d'une bibliothèque municipale

## Objectif
L'application développée pour la bibliothèque doit permettre la gestion des ressources et de leurs emprunts, la gestion des comptes des adhérents, ainsi que le recueillement de données sur les documents.
Ce système doit permettre de faciliter l'accès des diverses fonctionnalités aux utilisateurs.

## Besoins exprimés :

-   Faciliter aux adhérents la recherche de documents et la gestion de leurs emprunts.

-   Faciliter la gestion des ressources documentaires : ajouter des documents, modifier leur description, ajouter des exemplaires d'un document, etc.

-   Faciliter au personnel la gestion des prêts, des retards et des réservations.

-   Faciliter la gestion des utilisateurs et de leurs données.

-   Établir des statistiques sur les documents empruntés par les adhérents. Cela permettra par exemple d'établir la liste des documents populaires, mais aussi d'étudier le profil des adhérents pour pouvoir leur suggérer des documents.

## Livrables :

Les livrables sont disponibles à l'adresse suivante: https://gitlab.utc.fr/amiotnoe/nf17-td5-g5-biblio

-   README: Comporte le nom des membres du groupe
-   NDC: Note de Clarification
-   MCD: Modèle conceptuel de données
-   MLD: Modèle logique de données
-   BDD : tables et vues, données de test, questions attendues (vues)
-   Il faut intégrer dans le projet : héritage, contraintes, composition, vues, requêtes statistiques (agrégats), normalisation, transaction et optimisation.

## Fonctions de l'application :

⇒ Membre du personnel :
-   permettre de visualiser les ressources existantes    
-   permettre de modifier les éléments des tables du système (ajouter/supprimer)  
-   permettre de blacklister/pénaliser un utilisateur 
-   permettre la gestion des comptes d' utilisateurs (création et suppression de comptes, modifications des données personnelles)
-   permettre de consulter les statistiques calculées à partir des données

⇒ Utilisateur  :
-   permettre de visualiser les ressources existantes
-   permettre l'emprunt des ressources si l'utilisateur est adhérent
-   permettre de modifier ses données utilisateur 
-   consulter l'état de leur compte utilisateur
-   consulter leurs emprunts


## Les différents objets et leurs propriétés :

### Liste des objets identifiés:

-   Propriétaire de Compte
    -   Utilisateur
    -   Membre du personnel
-   Contributeur
    -   Nationalité
    -   Compositeur
    -   Interprète
    -   Acteur
    -   Réalisateur
    -   Auteur
-   Ressource
    -   Livre
    -   Film
    -   Enregistrement Musical
    -   Langue
    -   Genre
    -   Editeur

-   Remboursement
-   Prêt
-   Exemplaire
-   Adhésion

### Liste des attributs des objets
⇒ Propiétaire de Compte est une classe abstraite:
-   Login (clé) : varchar , on considère le login immuable pour chaque propriétaire de compte
-   Mot de passe : varchar
-   Nom : varchar
-   Prénom : varchar
-   Adresse : varchar
-   Adresse mail : varchar
Ici (nom,prenom,adresse), (nom,prenom,adressemail) est une clé candidate.

    ⇒ Utilisateur hérité de Propriétaire De Compte
	-   Numéro de téléphone : varchar
	-   Date de naissance : Date
	-   Date_Blacklist: date
	-   Il peut être ou avoir été adhérent.
	-   Il peut être blacklisté, auquel cas on gardera la date du moment où il a été blacklisté, si elle est à NULL c'est que l'utilisateur n'est pas blacklisté. On part du principe qu'un utilisateur peut être blacklisté une seule fois et un blacklist correspond à un utilisateur, d'où l'attribut.
	-   Un utilisateur ne pourra emprunter au maximum 5 ressources en même temps.

    ⇒ Membre du personnel hérité de Propriétaire De Compte
	-   C'est un propriétaire de compte sans attributs supplémentaires mais qui a des droits d'administration sur le système.
	-   Il pourra gérer les ressources et les utilisateurs.

On veut pouvoir garder une trace de toutes les adhésions, ainsi :


⇒ Adhésion est une classe liée à utilisateur (1:N)

-   login (clé) : clé étrangère vers la classe Utilisateur
-   Date de début (clé) : date
-   Date de fin : date
-   Un utilisateur peut en avoir plusieurs mais une seule est active à la fois.
-   L'adhésion donne le droit à un utilisateur d'emprunter et de consulter les ressources de la bibliothèque.
-   Une adhésion peut être actuelle ou passée.
    

⇒ Ressource est une classe abstraite :

-   Classe mère de : Livre, Film, et Enregistrement Musical
-   Code unique (clé) : int
-   Titre : varchar
-   Date d'apparition : date
-   Editeur : varchar
-   Genre : varchar
-   Code de classification : varchar (longueur normalisée) non considéré comme unique car indique seulement l'étage et l'étagère
	
	⇒ Genre : classe liée à Ressource (1:M) (relation 1:M car le cahier des charges indique “le genre” ce qu’on a interprété comme un seul genre par document)
	
	-   Genre (clé): varchar

	⇒ Editeur : classe liée à Ressource (1:M) (relation 1:M car le cahier des charges indique “l'éditeur” ce qu’on a interprété comme un seul editeur par document)
	
	-   Editeur (clé): varchar

	⇒ Livre : hérite de Ressource

	-   ISBN : int qui est considéré unique
	-   Résumé : varchar
	-   Langue : varchar
	-   Ajouter Attributs

	⇒ Film : hérite de Ressource

	-   Longueur : int (en minutes)
	-   Synopsis : varchar
	-   Langue : varchar
	-   Ajouter Attributs

	⇒ Enregistrement Musical : hérite de Ressource

	-   Longueur : int (en minutes)
	-   Ajouter Attributs
	
		⇒ Langue : classe liée à Film et Livres (1:N) car le cahier des charges indique “la langue des documents” ce qu’on a interprété comme une seule langue par document.

		-   Langue(clé): varchar
 
Ici une ressource ne peut pas appartenir à plusieurs catégories en même temps. 

⇒ Contributeur est une classe abstraite 

-   Id(clé) : int, clé artificielle car aucun groupe d'attributs ne satisfait les contraintes de simplicité et d'immuabilité dans cette classe
-   Nom : varchar
-   Prénom : varchar
-   Date de naissance : date
-   Nationalité: varchar

	⇒ Nationalité : classe liée à Contributeur (1:N) car le cahier des charges indique “sa nationalité” ce qu’on a interprété comme une seule nationalité par contributeur.
	
	-   Nationalité (clé): varchar

	⇒ Compositeur : hérite de Contributeur et est liée à Enregistrement Musical (N:M)

	⇒ Interprète : hérite de Contributeur et est liée à Enregistrement Musical (N:M)

	⇒ Acteur : hérite de Contributeur et est liée à Film (N:M)
	
	⇒ Réalisateur : hérite de Contributeur et est liée à Film (N:M)

	⇒ Auteur : hérite de Contributeur et est liée à Livre (N:M)

Ici on spécifie qu'un Contributeur peut arborer plusieurs catégories en même temps (auteur,acteur...)

   
⇒ Exemplaire est une composée avec Ressources (1:1..N), si la ressource n'existe plus, les exemplaires non plus.
-   Id_Exemplaire (clé locale) : int pour différencier plusieurs exemplaires d'une même ressource, elle devra être non nulle.
-   Etat: {neuf, bon, abîmé, perdu}
-   Prix: Float
-   Correspond à l'objet physique correspondant à une ressource, on peut donc avoir plusieurs exemplaires d'une même ressource.
-   On vérifiera que toutes les ressources possèdent des exemplaires.
    
Ici on définit le fait que le livre soit disponible par la biais d'une vue qui permettra d'établir la liste des ressources disponibles

⇒ Classe d'association Prêt entre Utilisateur (seulement les adhérents après identifications) et Exemplaire 
-   clé primaire formée des deux clés étrangères d'Utilisateur et d'Exemplaire et de la Date de prêt
-   Date de prêt: Date
-   Date de fin: Date
-   Date de rendu : Date
-   Etat de rendu : {neuf, bon, abîmé, perdu}
-   Le rendu de la ressource doit être pris en compte
-   Un document n'est emprunté que s'il est disponible et en bon état
-   Un adhérent sera blacklisté en cas de sanctions répétées
On devra vérifier pour faire un prêt que l’exemplaire est disponible, que l’utilisateur est un adhérent et qu'il n’a pas de sanctions ou de blacklist.
De plus l'exemplaire qui sera reservé ne devra pas déjà être réservé ou en prêt. 

Suite à un prêt, un adhérent sera sanctionné pour les retards dans le retour d'une ressource ou la dégradation de celle-ci.
Il faut donc gérer les sanctions :


⇒ Remboursement est une composée avec Prêt (1:0..1), un prêt peut mener à un remboursement ou non.
-   Date_Remboursement: Date
Le remboursement intervient en cas de détérioration ou de perte.
La sanction est maintenue jusqu'au remboursement.

 
De plus, on gère aussi les retards. Un retard provoque la suspension du droit de prêt d'une durée égale au nombre de jours de retard.
Nous le définirons comme une vue. 
On aura aussi une vue permettant de visionner les sanctions en cours. 

Pour ce qui est des réservations, nous avons décidé d'implémenter la classe suivante :

=> Classe association Réservation entre Utilisateur et Ressource (relation 1:N 0:N)
- Clé primaire formée des deux clés étrangères d'Utilisateur et de Ressource
- DateReservation: Date

On devra vérifier que pour faire une réservation, au moins un exemplaire est disponible, que l'utilisateur est un adhérent et n'a pas de sanction ou de blacklist.
De plus l'exemplaire qui sera reservé ne devra pas déjà être réservé ou en prêt. 
 

