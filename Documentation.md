# Liste des besoins ainsi que des commandes qui y répondent :
 
Dans ce document, on cherche à afficher des commandes possibles pour répondre aux besoins exprimés dans le cahier des charges et dans la NDC.

## Chercher un document :
On peut chercher un document de plusieurs façons différentes grâce à notre modélisation de la base de données de la bibliothèque.
On peut faire des recherches sur la longueur des films, la langue, l'IBSN et d'autres attributs présents dans les tables livre/film/ressource/musique.
- Parmis les livres/musiques/films : 

Ici ce n'est qu'un exemple qui permet d'afficher les livres dont le résumé contient le mot "Jean".
```sql
 SELECT * FROM vlivre WHERE resume LIKE '%Jean%';
```

- Parmis tous les documents :  
```sql
SELECT * FROM ressource WHERE titre like '%Joker%';
```

Ici on recherche parmis l'ensemble des ressources mélangées, un titre ("Joker") en particulier. Ce titre peut-être celui d'un livre, d'un film ou d'un enregistrement musical. On peut encore ici rechercher des ressources par rapport au titre, à la date d'apparition, le genre ou encore l'éditeur. 
 
- Par contributeur : 

```sql
SELECT * FROM vcontribution WHERE acteur IS NOT NULL;
```
```sql
SELECT * FROM vcontribution WHERE nom LIKE '%Zola%';
```
```sql
SELECT * FROM vcontribution WHERE nom LIKE '%Zola%' AND realisateur IS NOT NULL;
```

Ici on va chercher à afficher un document par rapport au contributeur. On va donc aller chercher dans la vue contribution (qui établit les contributions de chaque contributeur) un acteur ou un auteur en particulier. Par exemple dans la deuxième requête on recherche des ressources se rapportant à Zola. Dans la dernière on recherche les films réalisés par Zola. La première requête est plus générale et permet de lister tous les acteurs ainsi que les films dans lesquels ils ont joué. 

## Afficher et gérer des prêts/réservations :

### Prêt 

Ici on affiche l'ensemble des prêts réalisés par 'Phey1947'. On affiche toutes les informations sur le prêt et la ressource.

```sql
SELECT * FROM pret INNER JOIN ressource ON code=ressource WHERE utilisateur='Phey1947';
```

Cette requête ci-dessous est plus précise car elle permet seulement d'afficher les prêts en cours de 'Makeeires', c'est-à-dire les prêts dont la date de rendu est nulle.

```sql
SELECT * FROM pret INNER JOIN ressource ON code=ressource WHERE utilisateur='Makeeires' AND date_rendu IS NULL;
```

Afin d'ajouter un nouveau prêt, on insère dans la table prêt comme ci-dessous. Ici on met le code unique de la ressource ainsi que l'exemplaire emprunté. On indique l'utilisateur (ici 'Phey1947'), la date d'emprunt et la date normale de fin. Il faudra bien sûr vérifier que la ressource est disponible et que l'adhérent n'est pas sanctionné. 

```sql
INSERT INTO pret VALUES(1,1,'Phey1947','2020-05-12','2020-06-12');
```

Pour modifier un prêt (notament pour indiquer que l’utilisateur a rendu le document) : 

```sql
UPDATE pret SET date_rendu='2020-05-30', etat_rendu='bon' WHERE ressource=1 AND exemplaire=2 AND utilisateur='Phey1947' AND date_pret='2020-05-12';
```

On indique donc que le prêt précédement ajouté a été rendu le "30/05/2020" dans un bon état.

### Réservation

Avec les réservations nous avons plusieurs possibilités:

Visualiser les réservations :

```sql
SELECT * FROM reservation INNER JOIN ressource ON code=ressource;
```

Ajouter une ressource : 

```sql
INSERT INTO reservation VALUES('jwick34', 8, '2020-04-04');
```

Attention, il faut vérifier que le livre est disponible avant de créer une réservation !
Quand l’utilisateur qui avait réservé emprunte le livre, il faut supprimer la réservation : 

```sql
DELETE FROM reservation WHERE utilisateur='jwick34' AND ressource=8;
```

et rajouter l'emprunt dans prêt comme indiqué plus haut.


## Gestion des ressources de la bibliothèque :

Il est possible pour le personnel de gérer les ressources, les requêtes suivantes en sont la preuve.

La requête ci-dessous ajoute une ressource, un enregistrement musical de genre classique édité par Vevo. Son titre est 'Une chanson douce'. Il a été réalisé dans les années 2000. Son code de classification est 501.

```sql
INSERT INTO ressource VALUES(15, 'Une chanson douce', '2000-05-16', '501', 'Classique', 'Vevo');
```

Il est aussi possible de modifier une ressource. Ici on modifie le titre de la ressource ajoutée précédement pour lui rajouter un 's'.

```sql
UPDATE ressource SET titre = 'Une chanson douces' WHERE code = 15;
```

En plus de la table ressource, on peut aussi gérer celles des exemplaires afin de répertorier les différents exemplaires d'une ressource. Ici on ajoute à l'enregistrement 'Une chanson douce', un exemplaire à 12€50 dans l'état neuf.

```sql
INSERT INTO exemplaire VALUES(15, 1, 'neuf', '12.50');
```

## Gestion des sanctions des utilisateurs : 

Il est important dans le cadre d'une bibliothèque de gérer la validité d'un utilisateur adhérent à emprunter une ressource.

On peut visualier l'ensemble des retards à l'aide de la requête suivante :

```sql
SELECT * FROM vretard;
```

On peut aussi visualiser seulement les retards en cours, c'est-à-dire les retards où la ressource n'a pas encore été rendue.  

```sql
SELECT * FROM vretard LEFT JOIN ressource ON code=ressource WHERE date_rendu IS NULL;
```

Il faut aussi pouvoir visualiser les utilisateurs actuellement sanctionnés, la requête suivante le permet :

```sql
SELECT * FROM vsanction;
```

Attention, avant d’ajouter un prêt, il faut vérifier que l’utilisateur n’est pas sanctionné.  
De même, il faut vérifier que l’exemplaire est disponible :
```sql 
SELECT * FROM vdisponible;
```

## Visualisation des utilisateurs

Pour voir tous les utilisateur, on peut utiliser la requête suivante : 

```sql
SELECT * FROM utilisateur;
```

Pour modifier un utilisateur, on peut utiliser la requête suivante : 

```sql
UPDATE utilisateur SET nom='Satord' WHERE login='Prith1953';
```

Pour bannir un utilisateur, on peut utiliser la requête suivante : 

```sql
UPDATE utilisateur SET date_blackliste=current_date WHERE login='Prith1953';
```

Pour ajouter une adhésion à un utilisateur (Il ne peut y en avoir qu'une d'active à la fois !) :
```sql
INSERT INTO adhesion VALUES('Orwits', '1994-12-03', '1995-10-03');
```

Pour ce qui est des adhésions, on utilise la vue vAdhérent
Par exemple la visualisation des adhérents se fait par :

```sql
SELECT * FROM vAdherent;
```


## Création d'utilisateur dans la base de donnée :

Afin de pouvoir correctement gérer cette base de données, il faut que le personnel puissent s'en occuper. Les utilisateurs doivent aussi avoir des droits de visibilté sur certaines tables:

Pour créer un utilisateur de la base de données :

```sql
CREATE USER 'nom_utilisateur' IDENTIFIED BY 'mot_de_passe'
```

On peut ensuite lui attribuer plusieurs droits possibles :

```sql
GRANT Select,Insert ON 'Noms_tables' TO 'nom_utilisateur' 
``` 

Ici l'utilisateur 'nom_utilisateur' aura le droit d'insérer des données ainsi que de lire les données dans les tables/vues nommées, cela peut-être utile pour les membres du personnel par exemple. Si on veut seulement qu'un utilisateur ait accès en lecture, on enlève 'Insert'. Il faudra évidemment ne pas donner le droit de lecture à un adhérent sur la table personnel ou utilisateur. Il faut choisir. 

A l'inverse, la révocation de droit est aussi possible:

```sql
REVOKE Select,Insert ON 'Noms_tables' TO 'nom_utilisateur'
```

L'utilisateur 'nom_utilisateur' verra ses droits de lecture et d'insertion de données enlevés. 

### Utilisateur

L'utilisateur n'aura que des droits en lecture. Ainsi il aurait le droit en lecture sur toutes les tables mis à part la table __Utilisateur__ et __Personnel__ car il n'a pas d'intérêt à voir les autres utilisateurs et leur mot de passe. Ceci pourrait causer des problèmes. Il n'a pas accès en lecture sur les tables __Prêt__, __Adhésion__ et __Remboursement__ non plus. L'utilisateur n'a pas d'intérêt à visualiser les prêts des autres et leurs remboursements. 
Pour ce qui est des vues, il peut avoir accès toujours en lecture à la vue __vlivre__,__vfilm__ et __vmusique__ afin de visualiser les ressources par catégorie.
Il peut aussi accéder à __vcontribution__ s'il veut voir les contributeurs pour chaque ressource si cela peut l'aider à choisir. 


## Faire des recommandations à l'utilisateur :

Les exemples suivants donnent des idées de recommandations possiblement faites à l'utilisateur (Remplacez les deux occurences du login de l’utilisateur) :

__Exemple 1 :__

```sql
SELECT ressource.titre, ressource.genre
FROM   ressource LEFT JOIN pret
ON ressource.code = pret.ressource
WHERE  ressource.code
NOT IN (SELECT ressource FROM pret
WHERE  utilisateur = 'jwick34')
AND genre
IN (SELECT genre FROM pret LEFT JOIN ressource
ON ressource.code = pret.ressource
WHERE  utilisateur = 'jwick34'
GROUP  BY utilisateur, genre
ORDER  BY utilisateur, Count(*) DESC LIMIT  4)
GROUP  BY ressource.code, genre
ORDER  BY Count(*) DESC LIMIT 5;
```

Cette requête affiche les ouvrages que l’utilisateur n’a jamais empruntés parmis les plus empruntés dans les genres qu’il préfère.

Si le résultat de cette requête est vide (il a déjà emprunté tous les documents du même genre), la requête suivante peut lui faire une proposition plus générique (non basée sur les genres qu’il préfère):

__Exemple 2 :__
```sql
SELECT ressource.titre, ressource.genre
FROM   ressource LEFT JOIN pret
ON ressource.code = pret.ressource
WHERE  ressource.code
NOT IN (SELECT ressource
FROM   pret
WHERE  utilisateur = 'jwick34')
GROUP  BY ressource.code, genre
ORDER  BY Count(*) DESC LIMIT 5;
```

__Exemple 3 :__

Afficher les 5 ouvrages les plus empruntés : 
```sql
SELECT ressource.titre, ressource.genre, count(*) as nombreEmprunts
FROM   ressource LEFT JOIN pret 
ON ressource.code = pret.ressource
GROUP  BY ressource.code, genre
ORDER  BY Count(*) DESC LIMIT 5;
```

__Exemple 4 :__

Pour connaître les 4 genres favoris d’un utilisateur : 
```sql
SELECT genre, count(*) as nbr
FROM   pret
LEFT JOIN ressource
ON ressource.code = pret.ressource
WHERE  utilisateur = 'jwick34'
GROUP  BY utilisateur, genre
ORDER  BY utilisateur, Count(*) DESC LIMIT  4;

```
