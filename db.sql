--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 11.7 (Ubuntu 11.7-0ubuntu0.19.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: etats_possibles; Type: TYPE; Schema: public; Owner: bdd1p109
--

CREATE TYPE public.etats_possibles AS ENUM (
    'neuf',
    'bon',
    'abime',
    'perdu'
);


--
-- Name: trg_date_not_between_check(); Type: FUNCTION; Schema: public; Owner: bdd1p109
--

CREATE FUNCTION public.trg_date_not_between_check() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF EXISTS (SELECT utilisateur FROM adhesion WHERE adhesion.utilisateur=NEW.utilisateur 
AND ((NEW.date_debut BETWEEN adhesion.date_debut AND adhesion.date_fin OR NEW.date_fin BETWEEN adhesion.date_debut AND adhesion.date_fin) OR (adhesion.date_debut BETWEEN NEW.date_debut AND NEW.date_fin OR adhesion.date_fin BETWEEN NEW.date_debut AND NEW.date_fin))) THEN
RAISE EXCEPTION 'La periode de validite est comprise dans une adhesion deja existante pour cet utilisateur';
END IF;
RETURN NEW;
END
$$;

--
-- Name: trg_ressource_film_ok(); Type: FUNCTION; Schema: public; Owner: bdd1p109
--

CREATE FUNCTION public.trg_ressource_film_ok() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

BEGIN

IF EXISTS (SELECT ressource  FROM livre WHERE livre.ressource=NEW.ressource)

THEN

RAISE EXCEPTION 'Cette ressource est déjà enregistrée comme étant un livre';

END IF;

IF EXISTS (SELECT ressource  FROM musique WHERE musique.ressource=NEW.ressource)

THEN

RAISE EXCEPTION 'Cette ressource est déjà enregistrée comme étant une musique';

END IF;

RETURN NEW;

END

$$;


--
-- Name: trg_ressource_livre_ok(); Type: FUNCTION; Schema: public; Owner: bdd1p109
--

CREATE FUNCTION public.trg_ressource_livre_ok() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

BEGIN

IF EXISTS (SELECT ressource  FROM musique WHERE musique.ressource=NEW.ressource)

THEN

RAISE EXCEPTION 'Cette ressource est déjà enregistrée comme étant une musique';

END IF;

IF EXISTS (SELECT ressource  FROM film WHERE film.ressource=NEW.ressource)

THEN

RAISE EXCEPTION 'Cette ressource est déjà enregistrée comme étant un film';

END IF;


RETURN NEW;

END

$$;


--
-- Name: trg_ressource_musique_ok(); Type: FUNCTION; Schema: public; Owner: bdd1p109
--

CREATE FUNCTION public.trg_ressource_musique_ok() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

BEGIN

IF EXISTS (SELECT ressource  FROM livre WHERE livre.ressource=NEW.ressource)

THEN

RAISE EXCEPTION 'Cette ressource est déjà enregistrée comme étant un livre';

END IF;

IF EXISTS (SELECT ressource  FROM film WHERE film.ressource=NEW.ressource)

THEN

RAISE EXCEPTION 'Cette ressource est déjà enregistrée comme étant un film';

END IF;


RETURN NEW;

END

$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adhesion; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.adhesion (
    utilisateur character varying(60) NOT NULL,
    date_debut date NOT NULL,
    date_fin date NOT NULL,
    CONSTRAINT adhesions_check CHECK ((date_debut < date_fin))
);


--
-- Name: compose; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.compose (
    contributeur integer NOT NULL,
    musique integer NOT NULL
);


--
-- Name: contributeur; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.contributeur (
    id integer NOT NULL,
    nom character varying(60),
    prenom character varying(60),
    datenaissance date,
    nationalite character varying(60),
    CONSTRAINT check_nom_prenom CHECK (((nom IS NOT NULL) OR (prenom IS NOT NULL)))
);



--
-- Name: ecrit; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.ecrit (
    contributeur integer NOT NULL,
    livre integer NOT NULL
);



--
-- Name: editeur; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.editeur (
    nom character varying(60) NOT NULL
);



--
-- Name: exemplaire; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.exemplaire (
    ressource integer NOT NULL,
    id integer NOT NULL,
    etat public.etats_possibles NOT NULL,
    prix real NOT NULL
);



--
-- Name: film; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.film (
    ressource integer NOT NULL,
    longueur integer NOT NULL,
    synopsis text NOT NULL,
    langue character varying(60) NOT NULL
);



--
-- Name: genre; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.genre (
    nom character varying(60) NOT NULL
);



--
-- Name: interprete; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.interprete (
    contributeur integer NOT NULL,
    musique integer NOT NULL
);



--
-- Name: joue; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.joue (
    contributeur integer NOT NULL,
    film integer NOT NULL
);



--
-- Name: langue; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.langue (
    nom character varying(60) NOT NULL
);



--
-- Name: livre; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.livre (
    ressource integer NOT NULL,
    isbn integer NOT NULL,
    resume text NOT NULL,
    langue character varying(60) NOT NULL
);



--
-- Name: musique; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.musique (
    ressource integer NOT NULL,
    longueur integer NOT NULL
);



--
-- Name: nationalite; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.nationalite (
    nom character varying(60) NOT NULL
);



--
-- Name: personnel; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.personnel (
    login character varying(60) NOT NULL,
    mdp character varying(60),
    nom character varying(60),
    prenom character varying(60),
    adresse character varying(120),
    adresse_mail character varying(120)
);



--
-- Name: pret; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.pret (
    ressource integer NOT NULL,
    exemplaire integer NOT NULL,
    utilisateur character varying(60) NOT NULL,
    date_pret date NOT NULL,
    date_fin date NOT NULL,
    date_rendu date,
    etat_rendu public.etats_possibles,
    CONSTRAINT check_date CHECK ((date_pret <= date_fin))
);



--
-- Name: realise; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.realise (
    contributeur integer NOT NULL,
    film integer NOT NULL
);


--
-- Name: remboursement; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.remboursement (
    ressource integer NOT NULL,
    exemplaire integer NOT NULL,
    utilisateur character varying(60) NOT NULL,
    date_pret date NOT NULL,
    date_remboursement date
);



--
-- Name: reservation; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.reservation (
    utilisateur character varying(60) NOT NULL,
    ressource integer NOT NULL,
    date_reservation date
);



--
-- Name: ressource; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.ressource (
    code integer NOT NULL,
    titre character varying(120) NOT NULL,
    date_apparition date NOT NULL,
    code_classification character varying(60) NOT NULL,
    genre character varying(60) NOT NULL,
    editeur character varying(60) NOT NULL
);



--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: bdd1p109
--

CREATE TABLE public.utilisateur (
    login character varying(60) NOT NULL,
    mdp character varying(60) NOT NULL,
    nom character varying(60) NOT NULL,
    prenom character varying(60) NOT NULL,
    numero_telephone integer NOT NULL,
    date_naissance date NOT NULL,
    adresse character varying(120) NOT NULL,
    adresse_mail character varying(120) NOT NULL,
    date_blackliste date
);



--
-- Name: vadherent; Type: VIEW; Schema: public; Owner: bdd1p109
--

CREATE VIEW public.vadherent AS
 SELECT utilisateur.login,
    utilisateur.mdp,
    utilisateur.nom,
    utilisateur.prenom,
    utilisateur.numero_telephone,
    utilisateur.date_naissance,
    utilisateur.adresse,
    utilisateur.adresse_mail,
    utilisateur.date_blackliste,
    adhesion.utilisateur,
    adhesion.date_debut,
    adhesion.date_fin
   FROM (public.utilisateur
     LEFT JOIN public.adhesion ON (((adhesion.utilisateur)::text = (utilisateur.login)::text)))
  WHERE (((adhesion.date_debut < ('now'::text)::date) AND (adhesion.date_fin > ('now'::text)::date)) AND (utilisateur.date_blackliste IS NULL));



--
-- Name: vcontribution; Type: VIEW; Schema: public; Owner: bdd1p109
--

CREATE VIEW public.vcontribution AS
 SELECT ressource.code,
    ressource.titre,
    contributeur.nom,
    contributeur.prenom,
    contributeur.datenaissance,
    contributeur.nationalite,
        CASE
            WHEN (joue.contributeur IS NOT NULL) THEN 'Oui'::text
            ELSE NULL::text
        END AS acteur,
        CASE
            WHEN (compose.contributeur IS NOT NULL) THEN 'Oui'::text
            ELSE NULL::text
        END AS compositeur,
        CASE
            WHEN (interprete.contributeur IS NOT NULL) THEN 'Oui'::text
            ELSE NULL::text
        END AS interprete,
        CASE
            WHEN (realise.contributeur IS NOT NULL) THEN 'Oui'::text
            ELSE NULL::text
        END AS realisateur,
        CASE
            WHEN (ecrit.contributeur IS NOT NULL) THEN 'Oui'::text
            ELSE NULL::text
        END AS auteur
   FROM ((((((public.ressource
     LEFT JOIN public.ecrit ON ((ecrit.livre = ressource.code)))
     LEFT JOIN public.joue ON ((joue.film = ressource.code)))
     LEFT JOIN public.realise ON ((realise.film = ressource.code)))
     LEFT JOIN public.compose ON ((compose.musique = ressource.code)))
     LEFT JOIN public.interprete ON ((interprete.musique = ressource.code)))
     LEFT JOIN public.contributeur ON ((((((joue.contributeur = contributeur.id) OR (ecrit.contributeur = contributeur.id)) OR (realise.contributeur = contributeur.id)) OR (compose.contributeur = contributeur.id)) OR (interprete.contributeur = contributeur.id))));



--
-- Name: vdisponible; Type: VIEW; Schema: public; Owner: bdd1p109
--

CREATE VIEW public.vdisponible AS
 SELECT exemplaire.ressource,
    exemplaire.id
   FROM public.exemplaire
  WHERE ((exemplaire.etat <> ALL (ARRAY['abime'::public.etats_possibles, 'perdu'::public.etats_possibles])) AND (NOT (exemplaire.ressource IN ( SELECT reservation.ressource
           FROM public.reservation))))
EXCEPT
 SELECT pret.ressource,
    pret.exemplaire AS id
   FROM public.pret
  WHERE (pret.date_rendu IS NULL);



--
-- Name: vretard; Type: VIEW; Schema: public; Owner: bdd1p109
--

CREATE VIEW public.vretard AS
 SELECT pret.ressource,
    pret.exemplaire,
    pret.utilisateur,
    pret.date_pret,
    pret.date_fin,
    pret.date_rendu,
    pret.etat_rendu
   FROM public.pret
  WHERE (((pret.date_fin < ('now'::text)::date) AND (pret.date_rendu IS NULL)) OR ((pret.date_rendu > pret.date_fin) AND (pret.date_rendu IS NOT NULL)));



--
-- Name: vsanction; Type: VIEW; Schema: public; Owner: bdd1p109
--

CREATE VIEW public.vsanction AS
 SELECT vretard.utilisateur
   FROM public.vretard
  WHERE ((vretard.date_rendu + (vretard.date_rendu - vretard.date_fin)) > ('now'::text)::date)
UNION
 SELECT remboursement.utilisateur
   FROM public.remboursement
  WHERE (remboursement.date_remboursement IS NULL)
  GROUP BY remboursement.utilisateur;



--
-- Name: vemprunteur_potentiel; Type: VIEW; Schema: public; Owner: bdd1p109
--

CREATE VIEW public.vemprunteur_potentiel AS
 SELECT vadherent.login
   FROM public.vadherent
EXCEPT
 SELECT vadherent.login
   FROM (public.vadherent
     LEFT JOIN public.pret ON (((pret.utilisateur)::text = (vadherent.login)::text)))
  WHERE ((pret.date_rendu IS NULL) AND (pret.utilisateur IS NOT NULL))
  GROUP BY vadherent.login
 HAVING (count(*) >= 5)
EXCEPT
 SELECT vsanction.utilisateur AS login
   FROM public.vsanction;



--
-- Name: vfilm; Type: VIEW; Schema: public; Owner: bdd1p109
--

CREATE VIEW public.vfilm AS
 SELECT film.ressource,
    film.longueur,
    film.synopsis,
    film.langue,
    ressource.code,
    ressource.titre,
    ressource.date_apparition,
    ressource.code_classification,
    ressource.genre,
    ressource.editeur
   FROM (public.film
     LEFT JOIN public.ressource ON ((ressource.code = film.ressource)));



--
-- Name: vlivre; Type: VIEW; Schema: public; Owner: bdd1p109
--

CREATE VIEW public.vlivre AS
 SELECT livre.ressource,
    livre.isbn,
    livre.resume,
    livre.langue,
    ressource.code,
    ressource.titre,
    ressource.date_apparition,
    ressource.code_classification,
    ressource.genre,
    ressource.editeur
   FROM (public.livre
     LEFT JOIN public.ressource ON ((ressource.code = livre.ressource)));



--
-- Name: vmusique; Type: VIEW; Schema: public; Owner: bdd1p109
--

CREATE VIEW public.vmusique AS
 SELECT musique.ressource,
    musique.longueur,
    ressource.code,
    ressource.titre,
    ressource.date_apparition,
    ressource.code_classification,
    ressource.genre,
    ressource.editeur
   FROM (public.musique
     LEFT JOIN public.ressource ON ((ressource.code = musique.ressource)));



--
-- Data for Name: adhesion; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.adhesion (utilisateur, date_debut, date_fin) FROM stdin;
Terestand	2018-05-06	2018-06-07
Terestand	2018-06-08	2019-01-18
Terestand	2019-02-05	2019-04-08
Terestand	2019-05-20	2022-05-06
Orwits	2017-05-06	2017-07-19
Orwits	2018-09-29	2018-10-20
Orwits	2019-10-10	2023-06-08
Poer1979	2014-09-01	2014-12-04
Poer1979	2015-01-02	2015-09-06
Poer1979	2019-06-28	2020-04-29
Prith1953	2010-06-05	2014-05-06
Prith1953	2019-06-05	2024-06-15
Prith1953	2007-01-01	2007-01-02
Phey1947	1975-03-25	2015-04-23
Phey1947	2015-05-23	2025-05-28
Makeeires	1972-12-29	2020-04-27
jwick34	1989-04-05	2009-05-16
jwick34	2014-11-17	2022-06-18
\.


--
-- Data for Name: compose; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.compose (contributeur, musique) FROM stdin;
13	12
20	13
21	13
24	14
25	12
\.


--
-- Data for Name: contributeur; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.contributeur (id, nom, prenom, datenaissance, nationalite) FROM stdin;
1	Diesel	Vin	1953-10-26	Américain
2	Walker	Paul	1973-09-12	Américain
3	Dwayne	Johnson	1953-10-28	Américain
4	Depp	Johnny	1963-06-09	Américain
5	Stark	Tony	1975-09-06	Américain
6	Johansson	Scarlett	1975-09-03	Américain
7	Cotillard	Marion	1975-09-30	Français
8	Cassel 	Vincent	1966-11-23	Français
9	Zola	Emile	1840-04-02	Français
10	Hugo 	Victor	1885-05-22	Français
11	Vernes	Jules	1840-04-02	Français
12	Philips	Todd	1990-04-02	Américain
13	Zimmer	Hanz	1992-04-02	Américain
14	Wiliams	John	1840-05-02	Américain
15	Gim's	Maitre	1990-05-02	Français
16	Holland	Tom	1990-05-03	Américain
17	\N	Hergé	1990-05-04	Américain
18	Scorsese	Martin 	1942-11-17	Américain
19	Christie	Agatha 	1890-09-18	Anglais
20	\N	Slimane	1989-10-13	Français
21	\N	Vitaa	1983-03-14	Français
22	Tarantino	Quentin	1963-03-27	Américain
23	Bay	Michael	1965-02-17	Américain
24	\N	Nirvana	1987-01-01	Américain
25	\N	Angèle	2000-10-02	Français
\.


--
-- Data for Name: ecrit; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.ecrit (contributeur, livre) FROM stdin;
17	1
9	3
10	4
11	3
17	5
19	4
10	5
9	4
\.


--
-- Data for Name: editeur; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.editeur (nom) FROM stdin;
Hachette
Gallimard
Bayard
Atlas
France Loisirs
Panini
Larousse
Nathan
Le Monde
Universal
Vevo
DC Films
Winkler
Marvel Studios
Walt Disney
Illimunated Star Projection
\.


--
-- Data for Name: exemplaire; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.exemplaire (ressource, id, etat, prix) FROM stdin;
1	1	perdu	12.8000002
1	2	abime	11.5
3	2	abime	3.55999994
4	3	neuf	34.6699982
3	4	abime	34.5
5	5	neuf	45.9000015
4	6	bon	70.9000015
3	7	neuf	234.339996
5	8	perdu	34.5
4	9	neuf	34.7000008
8	1	neuf	25.3999996
8	2	bon	62.5
8	3	bon	50
8	4	bon	35.5
7	2	bon	35.5
\.


--
-- Data for Name: film; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.film (ressource, longueur, synopsis, langue) FROM stdin;
6	125	Ils font des courses de voiture	Anglais
8	176	Le fils du roi des animaux vient de naître. Son oncle est jaloux car il aimerait lui aussi régner. Il invente une ruse qui aboutira sur la mort du roi. Le jeune fils, Simba, témoin de la mort de son père, et s'en croyant responsable, va fuir sa horde	Anglais
9	129	Frank Sheeran est un ancien soldat de la Seconde Guerre mondiale devenu escroc et tueur à gages. À travers son personnage, on découvre le monde du crime organisé dans l'Amérique de l'après-guerre. Le film relate la disparition du légendaire dirigeant syndicaliste Jimmy Hoffa, qui reste l'un des mystères insondables de l'histoire de États-Unis.	Français
10	180	L'araignée sympa du quartier décide de rejoindre ses meilleurs amis Ned, MJ, et le reste de la bande pour des vacances en Europe. Cependant, le projet de Peter de laisser son costume de super-héros derrière lui pendant quelques semaines est rapidement compromis quand il accepte à contrecoeur d'aider Nick Fury à découvrir le mystère de plusieurs attaques de créatures, qui ravagent le continent !	Espagnol
7	599	Dans les années 1980, à Gotham City, Arthur Fleck, un comédien de stand-up raté est agressé alors qu'il ère dans les rues de la ville déguisé en clown. Méprisé de tous et bafoué, il bascule peu à peu dans la folie pour devenir le Joker, un dangereux tueur psychotique.	Italien
\.


--
-- Data for Name: genre; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.genre (nom) FROM stdin;
Policier
Thriller
Action
Romance
Science-Fiction
Comédie
Drames
Horreur
Comédie Musicale
Classique
Rap
Métal
Pop
Jazz
Funk
Gospel
Roman
Bande déssinée
Film d'animation
Gangsters
Variété
Rock
Super-Heros
\.


--
-- Data for Name: interprete; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.interprete (contributeur, musique) FROM stdin;
14	12
20	13
21	13
24	14
25	12
\.


--
-- Data for Name: joue; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.joue (contributeur, film) FROM stdin;
3	6
2	6
1	6
5	10
7	6
7	10
9	9
\.


--
-- Data for Name: langue; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.langue (nom) FROM stdin;
Français
Anglais
Allemand
Espagnol
Portugais
Chinois
Japonais
Italien
\.


--
-- Data for Name: livre; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.livre (ressource, isbn, resume, langue) FROM stdin;
1	3070773	Jean Valjean, ancien forçat, frappé par la générosité de Mgr Myriel à son égard, essaie désormais de faire le bien autour de lui, mais son passé et l’inspecteur Javert le rattrapent malgré lui. Évadé de prison, Jean Valjean sauve la fille de Fantine, la petite Cosette, des griffes des aubergistes Thénardier, et se dissimule avec elle dans un couvent. Quelques années plus tard, le jeune Marius tombe amoureux de Cosette. L’insurrection se prépare. Javert, surpris à espionner, est arrêté, mais Jean Valjean lui sauve la vie. « Déraillé », il se suicide. Le petit Gavroche meurt héroïquement sur les barricades. Jean Valjean sauve Marius, sans connaissance, à travers les égouts de Paris. Marius découvre qui est Jean Valjean et, ayant épousé Cosette, l’éloigne de ce forçat. Mais apprenant tardivement que ce forçat lui a sauvé la vie, il se rend avec Cosette auprès de lui : Jean Valjean se meurt, dans la solitude et le chagrin, mais peut du moins « mourir comme un saint », et heureux, ayant revu Cosette	Français
2	4489901	Tintin reporter au petit vingtième, repart en reportage, mais cette fois-ci en Afrique. Le voyage sur le navire est assez mouvementé, Milou manque de se noyer, Tintin aussi, mais finalement arrivent à bon port, où une foule les attend. A peine arrivés, non seulement on lui propose des sommes considérables pour l'exclusivité de ses reportages, mais quelques temps après son départ en safari, il se fait voler sa voiture, et on essaie de le tuer... Ayant réussi à mettre en fuite l'agresseur, Tintin repart pour faire son reportage, mais les aventures ne font que commencer...	Français
3	7703577	Le 24 mai 1863, le professeur Lidenbrock, minéralogiste au Johannaeum de Hambourg, et son neveu, Axel, découvrent un étrange grimoire signé d’un fameux alchimiste islandais du XVIe siècle: Arne Saknussemm. Ils y apprennent la possibilité d’atteindre le centre de la Terre. Ils se lancent aussitôt dans la folle entreprise, au mépris de toutes les objections scientifiques qu’elle soulève (chapitres 1-8). Les voici en Islande où, en compagnie de Hans, un guide dévoué et intrépide, ils s’enfoncent dans les entrailles d’un volcan éteint, le Sneffels, sur les traces d’Arne Saknussemm. Ils pénètrent alors dans un monde aussi fascinant que dangereux. Ils manquent mourir de soif, s’égarent dans un labyrinthe de galeries. Mais contre toute attente, au lieu de s’élever, la température des profondeurs terrestres se maintient à un degré parfaitement tolérable (chapitres 9-29). Ils traversent des forêts d’immenses champignons, de fougères et d’arbres primitifs, pleines d’extraordinaires fossiles. Ils naviguent même sur un océan souterrain, peuplé de monstres préhistoriques. Une affreuse tempête les jette sur un rivage, hanté par un troupeau de mastodontes gardé par un géant. La signature d’Arne Saknussemm les y attendait, gravée dans le roc. Le scepticisme d’Axel en est ébranlé, pendant que Lidenbrock se montre de plus en plus exalté (chapitres 30-40). Les explorateurs doivent s’ouvrir un passage à l’explosif. Entraînés sur un radeau dans la cheminée d’un volcan, ils sont ramenés par une éruption à la surface de la Terre. Ils se retrouvent ainsi, le 28 août, aux abords de l’île Stromboli, en Méditerranée, à «plus de douze cents lieues de leur point de départ». Lidenbrock connaît une gloire internationale, pendant qu’Axel, mûri par l’expérience, épouse Graüben, filleule du savant (chapitres 41-45). 	Français
4	6724693	Étienne Lantier, machineur sans travail, arrive un matin sur le carreau de la mine de Montsou. Mais la crise sévit et la mine n’embauche pas. Pourtant Maheu, un chef d’équipe qui a perdu une de ses herscheuses, lui propose de pousser les berlines de charbon. Pressé par la faim, Etienne accepte. Tassé contre Catherine Maheu lors de la descente, il l’a prise pour un garçon. Au fond, il découvre l’enfer de la taille où Maheu et ses trois haveurs extraient le charbon à « col tordu ». Lors de la pause, le jeune homme est confronté à Chaval, un mineur vindicatif qui, le voyant sympathiser avec Catherine, prend possession de celle-ci par un baiser. A la suite d’une inspection, l’équipe est mise à l’amende pour avoir négligé les boisages : payée au rendement, elle sacrifie la sécurité au nombre de berlines. Saisissant ce prétexte, l’ingénieur parle de réduire le prix de la berline et de payer le boisage à part. Remontés au jour, les mineurs ruminent leur révolte au cabaret de Rasseneur où s’installe Etienne. Après le mariage de son fils Zacharie, Maheu propose au jeune homme de le loger. C’est alors qu’Etienne, devenu haveur, convainc Maheu de créer à Montsou une caisse de prévoyance. Lorsque la compagnie baisse les salaires, la grève est décidée. L’argent de la caisse de prévoyance est bien vite épuisé, Maigrat, l’épicier, ne fait plus crédit, mais les mineurs, tiennent bon. Galvanisés par les rêves utopiques que leur prêche Etienne dans la forêt de Vandame, ils décident d’occuper les puits. Chaval, qui travaille désormais à Jean-Bart, un puits indépendant de la Compagnie, fait acclamer l’arrêt du travail dans sa fosse pour ne pas être en reste. Mais le lendemain, son patron, Deneulin, parvient à le retourner en le nommant porion*, et les ouvriers de Jean-Bart reprennent le travail. Ceux de Montsou, enragés d’avoir été trahis, coupent les câbles qui permettent la remontée des cages, et les jaunes, obligés de remonter par les échelles, sont enrôlés de force avec la foule des mineurs qui marchent sur Montsou. Etienne, gagné par une ivresse mauvaise, mène le saccage des fosses. Cependant, lorsque la foule s’en prend à l’hôtel de M. Hennebeau, il parvient à détourner sa colère contre Maigrat, qui se tue en tombant d’un toit et dont les femmes, furieuses d’avoir été si souvent ses proies sexuelles, émasculent le cadavre. Dans le coron, la misère souffle : chez les Maheu, Alzire, la petite infirme, est morte à force de privations ; en vain, car le travail reprend et l’armée occupe la fosse pour protéger les mineurs belges auxquels a fait appel la Compagnie. Quand les femmes commencent à jeter des briques sur la troupe, celle-ci riposte, tuant Maheu et six de ses camarades. - Domptés, les mineurs reprennent le travail en maudissant Etienne, auquel ils attribuent leurs malheurs, mais l’anarchiste Souvarine, méprisant leur résignation, sabote le cuvelage du puits pour détruire la mine. Lorsque Etienne et Catherine redescendent au Voreux, ils se retrouvent ainsi, prisonniers des galeries inondées avec Chaval. Après avoir tué son rival, Etienne possède enfin Catherine qui meurt dans ses bras. Quinze jours plus tard, les sauveteurs parviennent à dégager le jeune homme qui partira faire une carrière politique à Paris tandis que la Maheude redescend au fond	Allemand
5	2767503	Un soir, Hercule Poirot dîne dans un restaurant londonien. Sa table jouxte celle d'un jeune couple apparemment très épris, Jackie de Bellefort et Simon Doyle. Quelques semaines plus tard, à l'occasion d'une croisière sur le Nil, le grand détective a la surprise de retrouver Simon Doyle marié à Linnet Ridgeway. S'apercevant que Jackie s'ingénie à croiser le chemin du jeune couple, Hercule Poirot sent la tragédie venir à grands pas et il a peur... Tout au long de ce dramatique chassé-croisé amoureux, Poirot, moins orgueilleux qu'à l'accoutumée, fait montre d'une grande psychologie pour cerner chacun des personnages et tenter de raisonner ce trio de jeunes gens qui court à sa perte. Dans cette intrigue poignante au suspense haletant, on retrouve ce style inimitable, si typiquement anglais, et ces irrésistibles pointes d'humour tout en finesse signées Agatha Christie	Anglais
\.


--
-- Data for Name: musique; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.musique (ressource, longueur) FROM stdin;
12	213
13	223
14	270
\.


--
-- Data for Name: nationalite; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.nationalite (nom) FROM stdin;
Français
Anglais
Allemand
Américain
Espagnol
Portugais
Chinois
Japonais
Canadien
Brésilien
Argentin
Suisse
Italien
\.


--
-- Data for Name: personnel; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.personnel (login, mdp, nom, prenom, adresse, adresse_mail) FROM stdin;
Plimparthid	ohNohua8m	Laforge	Archard	76, rue Grande Fusterie 19100 BRIVE-LA-GAILLARDE	ArchardLaforge@teleworm.us
Enjusents	oofeiPhuCh7	Champagne	Odo	50, rue de Geneve 80090 AMIENS	OdoChampagne@teleworm.us
Haddepled	Ev2reaSh	Bélair	Saville	39, rue du Clair Bocage 33260 LA TESTE-DE-BUCH	SavilleBelair@dayrep.com
Judy1987	aigheekei5C	Lesage	Susanne	90, Place de la Madeleine 75010 PARIS	SusanneLesage@dayrep.com
\.


--
-- Data for Name: pret; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.pret (ressource, exemplaire, utilisateur, date_pret, date_fin, date_rendu, etat_rendu) FROM stdin;
1	1	Terestand	2020-03-04	2022-03-06	2020-04-03	perdu
1	2	Phey1947	2020-03-03	2021-04-07	2020-04-03	abime
8	1	Orwits	2020-03-04	2022-05-06	2020-04-01	abime
5	5	Makeeires	2020-03-05	2020-03-18	2020-04-01	neuf
3	4	Orwits	2020-03-04	2022-05-06	2020-03-25	perdu
5	5	jwick34	2020-04-01	2022-05-06	2020-04-02	bon
4	6	jwick34	2020-04-01	2022-05-06	2020-04-02	bon
3	7	Prith1953	2020-04-02	2022-05-06	2020-04-03	neuf
5	8	jwick34	2020-03-01	2022-05-06	2020-03-10	bon
4	9	jwick34	2020-04-01	2022-05-06	2020-04-03	bon
8	1	Phey1947	2020-04-01	2022-05-06	2020-04-03	perdu
5	8	Makeeires	2020-04-01	2048-12-01	2020-04-02	bon
5	8	Phey1947	2020-04-02	2048-12-01	2020-04-03	bon
5	5	Phey1947	2020-04-02	2022-05-06	2020-04-03	bon
8	1	Prith1953	2020-04-04	2020-09-08	\N	\N
4	3	Prith1953	2020-03-08	2020-09-08	\N	\N
4	6	Prith1953	2020-04-03	2020-09-08	\N	\N
4	9	Prith1953	2020-04-03	2020-09-08	\N	\N
5	5	Prith1953	2020-04-03	2020-09-08	\N	\N
3	7	Prith1953	2020-04-03	2020-09-08	\N	\N
8	2	Makeeires	2020-02-15	2020-04-01	\N	\N
8	3	jwick34	2019-02-02	2019-02-02	2020-04-02	bon
1	2	Phey1947	2020-05-12	2020-06-12	2020-05-30	bon
\.


--
-- Data for Name: realise; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.realise (contributeur, film) FROM stdin;
1	6
18	7
22	9
18	10
23	8
22	7
1	8
\.


--
-- Data for Name: remboursement; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.remboursement (ressource, exemplaire, utilisateur, date_pret, date_remboursement) FROM stdin;
1	1	Terestand	2020-03-04	2020-04-19
1	2	Phey1947	2020-03-03	2020-04-10
8	1	Orwits	2020-03-04	\N
\.


--
-- Data for Name: reservation; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.reservation (utilisateur, ressource, date_reservation) FROM stdin;
jwick34	4	2020-04-04
\.


--
-- Data for Name: ressource; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.ressource (code, titre, date_apparition, code_classification, genre, editeur) FROM stdin;
1	Les Misérables	2012-10-09	20	Roman	Hachette
2	Tintin au Congo	2010-09-05	25	Bande déssinée	Atlas
3	Voyage au Centre de la Terre	2011-08-15	10	Roman	Gallimard
4	Germinal	2005-02-19	2	Roman	Panini
5	Mort sur le Nil	2002-12-03	15	Roman	Larousse
6	Fast And Furious Hobbs And Shaw	2019-12-12	21	Action	Universal
10	Spider-Man: Far From Home	2019-06-28	2	Super-Heros	Marvel Studios
11	No Time For Caution	2014-04-23	101	Classique	Illimunated Star Projection
12	Ta reine	2016-02-05	102	Variété	Vevo
13	Avant toi	2019-02-03	103	Variété	Vevo
14	Smells Like Teen Spirit	1991-10-10	104	Rock	Vevo
7	Joker	2020-04-04	28	Thriller	DC Films
8	Le Roi Lion	2020-04-04	11	Film d'animation	Walt Disney
9	The Irishman	2019-02-02	5	Gangsters	Winkler
\.


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: bdd1p109
--

COPY public.utilisateur (login, mdp, nom, prenom, numero_telephone, date_naissance, adresse, adresse_mail, date_blackliste) FROM stdin;
Terestand	aiya7ioW	Foucault	Aimée	475758223	1980-10-15	43, Avenue des Pr'es 34000 MONTPELLIER\n	AimeeFoucault@dayrep.com	2020-03-02
Equied75	yaeree8eYoh	Lejeune	Emilie	192271780	1975-10-12	73, avenue de l'Amandier 92270 BOIS-COLOMBES	EmilieLejeune@armyspy.com	\N
jwick34	abcd1234	John	Wick	343456445	1980-01-15	113, rue du Président Roosevelt 77176 SAVIGNY-LE-TEMPLE	wickJohn@armyspy.com	\N
Makeeires	EgebaiMu9	Tisserand	Albracca	456991080	1972-09-02	99, Rue Marie De Médicis 69300 CALUIRE-ET-CUIRE	AlbraccaTisserand@teleworm.us	2019-12-15
Phey1947	Shai1Cee	Ménard	Yves	238693721	1947-01-07	71, rue du Président Roosevelt 49400 SAUMUR\n	YvesMenard@armyspy.com	2020-04-02
Poer1979	aqu8tieY0ie	Saurel	Clarice	535122683	1979-07-07	3, rue de Groussay 12000 RODEZ	ClariceSaurel@rhyta.com	\N
Orwits	Iegh4ca8p	Adler	Burnell	198923927	1992-06-25	58, boulevard Bryas 94000 CRÉTEIL	BurnellAdler@teleworm.us	2019-11-16
Therhavery	og5iThi5ooph	Desrosiers	Victorine	441889710	1997-12-20	97, rue du Fossé des Tanneurs 83000 TOULON	VictorineDesrosiers@armyspy.com	\N
Prith1953	Aun9giov3	Satord	Baril	313922253	1953-10-26	48, rue Petite Fusterie 62200 BOULOGNE-SUR-MER	SatordiBaril@teleworm.us	\N
\.


--
-- Name: adhesion adhesion_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.adhesion
    ADD CONSTRAINT adhesion_pkey PRIMARY KEY (utilisateur, date_debut, date_fin);


--
-- Name: compose compose_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.compose
    ADD CONSTRAINT compose_pkey PRIMARY KEY (contributeur, musique);


--
-- Name: contributeur contributeur_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.contributeur
    ADD CONSTRAINT contributeur_pkey PRIMARY KEY (id);


--
-- Name: ecrit ecrit_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.ecrit
    ADD CONSTRAINT ecrit_pkey PRIMARY KEY (contributeur, livre);


--
-- Name: editeur editeur_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.editeur
    ADD CONSTRAINT editeur_pkey PRIMARY KEY (nom);


--
-- Name: exemplaire exemplaire_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.exemplaire
    ADD CONSTRAINT exemplaire_pkey PRIMARY KEY (ressource, id);


--
-- Name: film film_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.film
    ADD CONSTRAINT film_pkey PRIMARY KEY (ressource);


--
-- Name: genre genre_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.genre
    ADD CONSTRAINT genre_pkey PRIMARY KEY (nom);


--
-- Name: interprete interprete_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.interprete
    ADD CONSTRAINT interprete_pkey PRIMARY KEY (contributeur, musique);


--
-- Name: joue joue_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.joue
    ADD CONSTRAINT joue_pkey PRIMARY KEY (contributeur, film);


--
-- Name: langue langue_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.langue
    ADD CONSTRAINT langue_pkey PRIMARY KEY (nom);


--
-- Name: livre livre_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.livre
    ADD CONSTRAINT livre_pkey PRIMARY KEY (ressource);


--
-- Name: musique musique_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.musique
    ADD CONSTRAINT musique_pkey PRIMARY KEY (ressource);


--
-- Name: nationalite nationalite_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.nationalite
    ADD CONSTRAINT nationalite_pkey PRIMARY KEY (nom);


--
-- Name: personnel personnel_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.personnel
    ADD CONSTRAINT personnel_pkey PRIMARY KEY (login);


--
-- Name: pret pret_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.pret
    ADD CONSTRAINT pret_pkey PRIMARY KEY (ressource, exemplaire, utilisateur, date_pret);


--
-- Name: realise realise_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.realise
    ADD CONSTRAINT realise_pkey PRIMARY KEY (contributeur, film);


--
-- Name: remboursement remboursement_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.remboursement
    ADD CONSTRAINT remboursement_pkey PRIMARY KEY (ressource, exemplaire, utilisateur, date_pret);


--
-- Name: reservation reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (utilisateur, ressource);


--
-- Name: ressource ressource_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.ressource
    ADD CONSTRAINT ressource_pkey PRIMARY KEY (code);


--
-- Name: contributeur unique_nom_prenom_naissance; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.contributeur
    ADD CONSTRAINT unique_nom_prenom_naissance UNIQUE (nom, prenom, datenaissance);


--
-- Name: utilisateur utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (login);


--
-- Name: film is_film_ok; Type: TRIGGER; Schema: public; Owner: bdd1p109
--

CREATE TRIGGER is_film_ok BEFORE INSERT ON public.film FOR EACH ROW EXECUTE PROCEDURE public.trg_ressource_film_ok();


--
-- Name: livre is_livre_ok; Type: TRIGGER; Schema: public; Owner: bdd1p109
--

CREATE TRIGGER is_livre_ok BEFORE INSERT ON public.livre FOR EACH ROW EXECUTE PROCEDURE public.trg_ressource_livre_ok();


--
-- Name: musique is_musique_ok; Type: TRIGGER; Schema: public; Owner: bdd1p109
--

CREATE TRIGGER is_musique_ok BEFORE INSERT ON public.musique FOR EACH ROW EXECUTE PROCEDURE public.trg_ressource_musique_ok();


--
-- Name: adhesion isdateok_check; Type: TRIGGER; Schema: public; Owner: bdd1p109
--

CREATE TRIGGER isdateok_check BEFORE INSERT ON public.adhesion FOR EACH ROW EXECUTE PROCEDURE public.trg_date_not_between_check();


--
-- Name: adhesion adhesion_utilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.adhesion
    ADD CONSTRAINT adhesion_utilisateur_fkey FOREIGN KEY (utilisateur) REFERENCES public.utilisateur(login);


--
-- Name: compose compose_contributeur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.compose
    ADD CONSTRAINT compose_contributeur_fkey FOREIGN KEY (contributeur) REFERENCES public.contributeur(id);


--
-- Name: compose compose_musique_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.compose
    ADD CONSTRAINT compose_musique_fkey FOREIGN KEY (musique) REFERENCES public.musique(ressource);


--
-- Name: contributeur contributeur_nationalite_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.contributeur
    ADD CONSTRAINT contributeur_nationalite_fkey FOREIGN KEY (nationalite) REFERENCES public.nationalite(nom);


--
-- Name: ecrit ecrit_contributeur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.ecrit
    ADD CONSTRAINT ecrit_contributeur_fkey FOREIGN KEY (contributeur) REFERENCES public.contributeur(id);


--
-- Name: ecrit ecrit_livre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.ecrit
    ADD CONSTRAINT ecrit_livre_fkey FOREIGN KEY (livre) REFERENCES public.livre(ressource);


--
-- Name: exemplaire exemplaire_ressource_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.exemplaire
    ADD CONSTRAINT exemplaire_ressource_fkey FOREIGN KEY (ressource) REFERENCES public.ressource(code);


--
-- Name: film film_langue_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.film
    ADD CONSTRAINT film_langue_fkey FOREIGN KEY (langue) REFERENCES public.langue(nom);


--
-- Name: film film_ressource_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.film
    ADD CONSTRAINT film_ressource_fkey FOREIGN KEY (ressource) REFERENCES public.ressource(code);


--
-- Name: interprete interprete_contributeur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.interprete
    ADD CONSTRAINT interprete_contributeur_fkey FOREIGN KEY (contributeur) REFERENCES public.contributeur(id);


--
-- Name: interprete interprete_musique_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.interprete
    ADD CONSTRAINT interprete_musique_fkey FOREIGN KEY (musique) REFERENCES public.musique(ressource);


--
-- Name: joue joue_contributeur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.joue
    ADD CONSTRAINT joue_contributeur_fkey FOREIGN KEY (contributeur) REFERENCES public.contributeur(id);


--
-- Name: joue joue_film_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.joue
    ADD CONSTRAINT joue_film_fkey FOREIGN KEY (film) REFERENCES public.film(ressource);


--
-- Name: livre livre_langue_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.livre
    ADD CONSTRAINT livre_langue_fkey FOREIGN KEY (langue) REFERENCES public.langue(nom);


--
-- Name: livre livre_ressource_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.livre
    ADD CONSTRAINT livre_ressource_fkey FOREIGN KEY (ressource) REFERENCES public.ressource(code);


--
-- Name: musique musique_ressource_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.musique
    ADD CONSTRAINT musique_ressource_fkey FOREIGN KEY (ressource) REFERENCES public.ressource(code);


--
-- Name: pret pret_ressource_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.pret
    ADD CONSTRAINT pret_ressource_fkey FOREIGN KEY (ressource, exemplaire) REFERENCES public.exemplaire(ressource, id);


--
-- Name: pret pret_utilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.pret
    ADD CONSTRAINT pret_utilisateur_fkey FOREIGN KEY (utilisateur) REFERENCES public.utilisateur(login);


--
-- Name: realise realise_contributeur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.realise
    ADD CONSTRAINT realise_contributeur_fkey FOREIGN KEY (contributeur) REFERENCES public.contributeur(id);


--
-- Name: realise realise_film_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.realise
    ADD CONSTRAINT realise_film_fkey FOREIGN KEY (film) REFERENCES public.film(ressource);


--
-- Name: remboursement remboursement_ressource_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.remboursement
    ADD CONSTRAINT remboursement_ressource_fkey FOREIGN KEY (ressource, exemplaire, utilisateur, date_pret) REFERENCES public.pret(ressource, exemplaire, utilisateur, date_pret);


--
-- Name: reservation reservation_ressource_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_ressource_fkey FOREIGN KEY (ressource) REFERENCES public.ressource(code);


--
-- Name: reservation reservation_utilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_utilisateur_fkey FOREIGN KEY (utilisateur) REFERENCES public.utilisateur(login);


--
-- Name: ressource ressource_editeur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.ressource
    ADD CONSTRAINT ressource_editeur_fkey FOREIGN KEY (editeur) REFERENCES public.editeur(nom);


--
-- Name: ressource ressource_genre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bdd1p109
--

ALTER TABLE ONLY public.ressource
    ADD CONSTRAINT ressource_genre_fkey FOREIGN KEY (genre) REFERENCES public.genre(nom);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--


--
-- PostgreSQL database dump complete
--
